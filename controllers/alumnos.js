const path = require('path');
const alumno = require('../models/alumnos');


exports.index = function (req, res) {
        res.redirect('/mostrar');
};

exports.agregar = function (req, res) {
        var newalumno= new alumno(req.body);
        console.log(req.body);
        newalumno.save(function (err) {
                if(err) {
                res.status(400).send('No se pudo guardar en la base de datos');
            } else {
                res.redirect('/mostrar');
            }
      });
                   };


exports.listar = function (req, res) {
        alumno.find({}).exec(function (err, alumnos) {
                if (err) {
                        return res.send(500, err);
                }
                res.render('index', {
                        alumnos: alumnos
             });
        });
};

