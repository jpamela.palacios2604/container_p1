# Contenido
Esta proyecto contiene aplicación web programada en NODEJS que lo que hace es agregar alumnos a una base de datos en MONGODB. Al momento de agregar los datos estos se muestran debajo.


Para mantener la aplicacion disponible se hace uso de contenedores, uno para que funcione el servidor web y otro para alojar la base de datos mongodb.

Para que esto se pueda manejar se usa docker composer, en el archivo docker-compose.yml se indica como este se conecta.