const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Alumno = new Schema ({
        nombre: { type: String, required: true },
        carnet: { type: String, required: true },
});

module.exports = mongoose.model('Alumno', Alumno)
