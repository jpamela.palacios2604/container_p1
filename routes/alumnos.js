const express = require('express');
const router = express.Router();
const al = require('../controllers/alumnos');

router.get('/', function(req, res){
    al.index(req,res);
});
router.get('/tarea', function(req, res){
    al.index(req,res);
});
router.post('/agregar', function(req, res) {
    al.agregar(req,res);
});

router.get('/mostrar', function(req, res) {
    al.listar(req,res);
});

module.exports = router;
