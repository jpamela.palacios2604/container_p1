const express = require('express');
const router = express.Router();
const path = require('path');
const al = require('../controllers/alumnos');

router.use (function (req,res,next) {
  console.log('/' + req.method);
  next();
});

router.get('/',function(req,res){
  al.index(req,res);
});

module.exports = router;
